CC  = /usr/local/cuda-10.0/bin/nvcc
LDFLAGS = -L /usr/local/cuda-10.0/lib64
IFLAGS 	= -I/usr/local/cuda-10.0/samples/common/inc -I/content/hpc/src

hello_world: hello_world.cu
	$(CC) hello_world.cu $(LDFLAGS) $(IFLAGS) -c $<
	$(CC) hello_world.o  $(LDFLAGS) $(IFLAGS) -o hello_world
